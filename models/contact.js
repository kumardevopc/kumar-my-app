const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

// initialize the auto-increment module
autoIncrement.initialize(mongoose.connection);

const NameSchema = mongoose.Schema({
    name: String,
    phone_number: String,
    email: String,
  }, { _id: false });


  const Contact = mongoose.model('Contact', mongoose.Schema({
    contact_id: {
        type: Number,
        unique: true,
        index: true,
        required: true,
    },
    name: {
      type: String,
      unique: true,
      index: true,
      required: true,
    },
    phone_number: {
        type: String,
        unique: true,
        index: true,
        required: true,
    },
    email: {
        type: String,
        unique: true,
        index: true,
        required: true,
    },
    updated_date: Date,
  }, { collection: 'contacts' })
    .plugin(autoIncrement.plugin, { model: 'contact', field: 'contact_id', startAt: 1 })
    .post('save', (error, doc, next) => {
      // this is the error code for all duplicate
      if (error) {
        console.log('error='+error);
      }
    }));
  
  module.exports = {
    Contact,
  };