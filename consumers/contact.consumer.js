const { initializeConsumer } = require('../services/rabbitMQService');

module.exports = {
    start: () => {
      let channelWrapper;
      const onMessage = async (data) => {
        let message;
        try {
          // try parsing the message
          message = JSON.parse(data.content.toString());
          console.log('Message recieved as below:');
          console.log('name = '+message.name+', phone number = '+message.phone_number+' and email = '+message.email);
        } catch (error) {
          console.log('error'+error);
        }
        channelWrapper.ack(data);
      };
  
      // initialize a consumer for the given queue that will call the provided callback
      channelWrapper = initializeConsumer('Notify-contact-queue', onMessage);
  
      // return the channel in case it needs to be accessed
      return channelWrapper;
    },
  };