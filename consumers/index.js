const contact = require('./contact.consumer');

module.exports = {
    // this function should include and start all consumers so we don't start each one manually on server.js
    start: () => {
      contact.start();
    },
  };