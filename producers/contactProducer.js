const queues = require('amqp-queues');
const { publishToQueue } = require('../services/rabbitMQService');

const sendQueue = (data) => {
    return publishToQueue('Notify-contact-queue',data);
  };

  module.exports = {
    sendQueue,
  };