const amqp = require('amqp-connection-manager');
const url = 'amqp://127.0.0.1:5672';

let ch = null;



const publishToQueue = (queueName, data) => {
    const connection = amqp.connect(url, { json: true });
    const ch = connection.createChannel({
        json: true,
        setup: channel => channel.assertQueue(queueName, { durable: true }),
    });

    return ch.sendToQueue(queueName, data, { contentType: 'application/json', persistent: true })
}

const initializeConsumer = (queueName, callback) => {
    const connection = amqp.connect(url, { json: true });
    return connection.createChannel({
        json: true,
        setup: channel => Promise.all([
          channel.assertQueue(queueName, { durable: true }),
          channel.prefetch(1),
          channel.consume(queueName, callback),
        ]),
      });
  };

  module.exports = { publishToQueue, initializeConsumer};