const { Contact } = require('../models/contact');
const contactProducer = require('../producers/contactProducer');

exports.add = async(req, res, next) => {
    const data = req.body;
    try {
        const contact_data = Object.assign({}, data);
        const savedContact = await Contact.create(contact_data);
        if (savedContact) {
            const queueResponse = await contactProducer.sendQueue(savedContact);
            if(queueResponse){
                return res.send('Queue sent successfully.');
            }else{
                return res.send('Queue could not be sent.');
            }
        }else{
            return res.send('Could not save successfully');
        }
    } catch (error) {
        return next(error);
    }
};