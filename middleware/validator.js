exports.addValidate = async(req, res, next) => {
    if (Object.keys(req.body).length > 0) {
        //more validation
        return next();
    }
    return res.send('No data received');
  };