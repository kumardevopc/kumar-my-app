var express = require('express');
var router = express.Router();

// import the controller
const contactController = require('../controllers/contactController');

const validator = require('../middleware/validator');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/list', function(req, res, next){
  res.send('list received');
});

router.post('/add', validator.addValidate, contactController.add);

module.exports = router;
